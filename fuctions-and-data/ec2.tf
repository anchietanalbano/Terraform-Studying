resource "aws_instance" "server" {
  count = local.instance_number <= 0 ? 0 : local.instance_number

  ami = var.instance_ami

  instance_type = lookup(var.instance_type, var.env)

  tags = merge(
    local.common_tags,

    {
      Project = "Curso terraform"
      Env     = format("%s", var.env) // parecido com o printf do C
      Name    = format("Instancia %d", count.index + 1)
    }
  )

}