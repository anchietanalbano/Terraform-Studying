variable "env" {}

variable "aws_region" {
  type        = string
  description = ""
}

variable "aws_profile" {
  type        = string
  description = ""
}

variable "instance_ami" {
  type        = string
  description = ""

  validation {
    condition = length(var.instance_ami) > 4 && substr(var.instance_ami, 0, 4) == "ami-"

    error_message = "Ami da instancia inválido"
  }

}

// quantidade de instancia a ser criada por tipo
variable "instance_number" {
  type = object({
    dev  = number
    prod = number
  })

  description = "Quantidade de instancia para criar"


}
variable "instance_type" {
  type = object({
    dev  = string
    prod = string
  })
  description = ""
  //default = {} valores passados em outro arquivo
  // porém poderia ser colocado t2.micro... 
}

