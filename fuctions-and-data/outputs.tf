output "ip_public_instance" {
  value = aws_instance.server.*.public_ip
  
}

output "name_instances" {
  value = join(", ", aws_instance.server.*.tags.Name)
}