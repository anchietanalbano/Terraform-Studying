//quando uma variavel se repeti demais mas precisa alterar um valor em determinado uso especifico


locals {
  ip_file = "ips.json"

  common_tags = {
    Service     = "Curso de terraform"
    ManagedBy   = "Terraform"
    Environment = var.environment
    Owner       = "Anchieta Albano"
  }
}