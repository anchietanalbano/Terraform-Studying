// Interpolação, junta variaveis e strings 

resource "aws_s3_bucket" "this" {
  bucket = "${random_pet.bucket.id}-${var.environment}"

  tags = local.common_tags
}

resource "aws_s3_object" "this" {
  bucket       = aws_s3_bucket.this.bucket
  key          = "config/${local.ip_file}"
  source       = local.ip_file
  content_type = "application/json"
  etag         = filemd5(local.ip_file)
}

resource "aws_s3_object" "random" {
  bucket       = aws_s3_bucket.this.bucket
  key          = "config/${random_pet.bucket.id}.json"
  source       = local.ip_file
  content_type = "application/json"
  etag         = filemd5(local.ip_file)
}