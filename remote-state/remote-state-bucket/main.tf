terraform {
  required_version = "1.8.5"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "5.51.1"
    }
  }
}

data "aws_caller_identity" "current" {}

provider "aws" {
  region  = "us-east-1"
  profile = "default"
}

resource "aws_s3_bucket" "remote-state" {
  bucket = "tfstate-${data.aws_caller_identity.current.account_id}"
  versioning {
    enabled = true
  }

  tags = {
    Description = "Local para armazenar arquivos do tfstate"
    ManageBy    = "Terraform"
    Owner       = "Anchieta Albano"
    CreatedAt   = "2021-06-24"
  }

}

output "remote_state_bucket" {
  value = aws_s3_bucket.remote-state.bucket
}

output "remote_state_bucket_arn" {
  value = aws_s3_bucket.remote-state.arn
}