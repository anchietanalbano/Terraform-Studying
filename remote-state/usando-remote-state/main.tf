terraform {
  required_version = "1.8.4"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "5.51.1"
    }
  }

  backend "s3" {}
}

provider "aws" {
  region  = "us-east-1"
  profile = "default"
}