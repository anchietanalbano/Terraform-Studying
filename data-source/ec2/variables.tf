variable "aws_region" {
  type        = string
  description = ""
  default     = "us-east-1"
}
variable "aws_profile" {
  type        = string
  description = ""
  default     = "default"
}
variable "environment" {
  type        = string
  description = ""
  default     = "dev"
}
variable "instance_type" {
  type        = string
  description = ""
  default     = "t2.micro"
}

variable "instance_tags" {
  type        = map(string)
  description = ""
  default = {
    Name    = "Ubuntu"
    Project = "Aws Terraform"
  }
}