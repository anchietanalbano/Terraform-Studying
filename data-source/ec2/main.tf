terraform {
  required_version = "1.8.5"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "5.51.1"
    }
  }
  //bucket = "tfstate-058264207549"
  //remote_state_bucket = "tfstate-058264207549"
  
  backend "s3" {
    bucket  = "tfstate-058264207549"
    key     = "dev/03-data-sources-s3/terraform.tfstate"
    region  = "us-east-1"
    profile = "default"
  }
}

provider "aws" {
  region  = var.aws_region
  profile = var.aws_profile
}