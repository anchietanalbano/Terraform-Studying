provider "aws" {
  region  = var.aws_region
  profile = var.aws_profile
}

/*
resource "aws_s3_bucket" "my-bucket-teste-One" {
  bucket = "my-tf-test-bucket-one-two-three-321"

  tags = {
    Name        = "My bucket"
    Environment = "Dev"
    Managedby   = "Terraform"
  }
}*/

resource "aws_instance" "web" {
  ami           = var.instance_ami
  instance_type = var.instance_type

  tags = var.instance_tags

}