output "extensions" {
  value = local.file_extensions
}

output "extensions_upper" {
  value = local.file_extensions_upper
}

output "instance_arns" {
  value = [for k, v in aws_instance.this : v.arn]
}

output "instance_names" {
  value = { for k, v in aws_instance.this : k => v.tags.Name }
}

output "private_ips" {
  // pode usar for no output
  value = [for o in local.ips : o.private]
}

output "public_ips" {
  // for splat no terraform
  value = local.ips[*].public
}
