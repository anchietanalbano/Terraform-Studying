
<h1 align="center"> Terraform Studying </h1>

Alguns passos em Terraform e conhecimentos importantes para o desenvolvimento de infraestrutura por códico IaC.

Conceitos de variaveis, interpolação, locals e outputs.

Estado da infraestrutura no tfstate, uma forma de ver organizadamente - 'terraform show'

O terraform possui uma forma de gerenciar recursos que não foram criados pelo mesmo, atravez do 'terraform import'.

obs: É necessario configurar o recurso no modulo principal.

É recomendado pela documentação que, salvemos o state da infraestrutura na nuvem para garanti o compartilhamento de dados para todos os membros da equipe.

Neste documento usaremos o recurso S3 do provider AWS, habilitando o versionamento do S3. 

Funções em terraform são bem simples de usar, a maior parte de exemplos de casos de uso também se encontrar na documentação oficial.

Além de funções complexas, também tem as mais simples como formatação de strings.

Funções `count` são interessantes de usar, mas, ao querer pegar informações do produto das mesmas, tem que se atentar que é uma lista logo usamos o ´*´.
Exemplo usando: `value = aws_instance.server.*.public_ip`.

Foi usando um pequeno exemplo de "data template" para criação de um arquivo zip, ultilizando o recurso ´archive_file`, que primeiro gera o arquivo localmente e logo depois alocar ao recurso S3.

A documentação da HashCorp usando o provider AWS -> https://registry.terraform.io/providers/hashicorp/aws/latest/docs

<h4 align="center">:construction: Projeto em construção :construction:</h4>